# Azure Agents

Warning: Replace "__library_chageme__" for your **library name** on
[vars.yml](.azure-pipelines/vars.yml).

Sample environments on Azure Library:

```env
AZP_URL=""                   # https://dev.azure.com/<organization-name>
AZP_POOL=""                  # Name Agent Pool
AZP_TOKEN=""                 # PAT (Personal Access Token)
POOL_ID="10"                 # Azure ID pool from https://dev.azure.com/<organization-name>/_settings/agentpools?poolId=10
DOCKER_IMG_URL=""            # Docker Azure Image eg. <ID>.dkr.ecr.us-east-1.amazonaws.com/<REPO_NAME>
DOCKER_TAG=""                # Docker Tag, by default: latest
DOCKER_IMAGE_PULL_POLICY=""  # Docker imagePullPolicy: Always, IfNotPresent, Never, by default: IfNotPresent

# AWS
AWS_REGION_NAME=""           # Var for only Azure Pipeline usage, eg. us-east-1
AWS_IAM_ROLE=""              # IAM Role of AWS eg. arn:aws:iam::<ID>:role/<name-role>
AWS_CONNECTION_NAME=""       # AWS Service Connection Name
```

## Requirements

### AWS

- [x] EKS v1.25
- [x] Keda v2.10.1

### K3S

- [x] Keda v2.10.1

## Usage in k8s

```console
cp -v .env.sample .env
```

```console
bash scripts/set-aws-vars.sh
```

```console
kubectl apply -f prod/aws
```

## Usage with Docker or Podman

```console
docker run -e AZP_URL=https://dev.azure.com/<NAME> \
           -e AZP_TOKEN=<YOUR_PAT_TOKEN> \
           -e AZP_POOL=linux-containers-aks \
           docker.io/<REPO>/az-agent:ubuntu-22.04
```

```console
podman run -e AZP_URL=https://dev.azure.com/<NAME> \
           -e AZP_TOKEN=<YOUR_PAT_TOKEN> \
           -e AZP_POOL=linux-containers-aks \
           docker.io/<REPO>/az-agent:ubuntu-22.04
```

## Logs

Check if azure-agent working

```console
kubectl -n keda logs pod/keda-operator-<HASH>
```

## Build

### Build Image via docker

```console
docker build --no-cache --tag az-agent:ubuntu-22.04 -f container/Dockerfile.amd64 container/
```

### Build Image via podman

```console
podman build --no-cache --tag az-agent:ubuntu-22.04 -f container/Dockerfile.amd64 container/
```

## KEDA's relationship with HPA

KEDA acts like a "Custom Metrics API" for exposing metrics to the HPA. KEDA
can't do its job without the HPA.

The complexity of developing a metrics server is abstracted away by using KEDA.

Scalers are the glue that provides the metrics from various sources to the HPA.

Here's a list of some of the most widely used scalers:

- [x] Apache Kafka
- [x] AWS CloudWatch
- [x] AWS Kinesis Stream
- [x] AWS SQS Queue
- [x] Azure Blob Storage
- [x] Azure Event Hubs
- [x] Azure Log Analytics
- [x] Azure Monitor
- [x] Azure Service Bus
- [x] Azure Storage Queue
- [x] Google Cloud Platform Pub/Sub
- [x] IBM MQ
- [x] InfluxDB
- [x] NATS Streaming
- [x] OpenStack Swift
- [x] PostgreSQL
- [x] Prometheus
- [x] RabbitMQ Queue
- [x] Redis Lists

For a complete list view the [scalers](https://keda.sh/docs/2.8/scalers/) section on the KEDA site.

A common question is when should one use a HPA and when to enlist KEDA.
If the workload is memory or cpu intensive, and has a well defined metric
that can be measured then using a HPA is sufficient.

When dealing with a workload that is event driven or relies upon a
custom metric, then using KEDA should be the first choice.

## References

- <https://learn.microsoft.com/en-us/training/modules/aks-app-scale-keda/6-concept-scaling-options>
- <https://keda.sh/docs/2.8/deploy/>

## License

This work is licensed under the [GNU GPLv3+](LICENSE)
